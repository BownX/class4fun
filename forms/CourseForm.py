# coding: utf-8

from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, DateField, IntegerField
from wtforms.validators import DataRequired, Email, Length, Regexp

class NewCourseForm(Form):
	""" validate for new course """
	title = StringField('title', validators=[DataRequired(), Length(min=2, max=50)])
	info = StringField('info', validators=[DataRequired(), Length(min=2, max=2000)])
	time = DateField('time', format='%m/%d/%Y', validators=[DataRequired()])
	location = StringField('location')
	mode = IntegerField('mode')
		