# coding: utf-8

import os
from flask import Flask, render_template
from flask_wtf.csrf import CsrfProtect
from flask.ext.login import LoginManager, login_required

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24).encode('hex')

loginManager = LoginManager()
loginManager.setup_app(app)
loginManager.login_view = "account.login"
CsrfProtect(app)

from views.Account import accountView
from views.Course import courseView

app.register_blueprint(accountView, url_prefix='/account')
app.register_blueprint(courseView, url_prefix='/course')

@app.route('/')
def index():
    return render_template("home_index.html",course_mode=-1)

@app.route('/home')
@login_required
def home():
	# no page available.
	pass

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404