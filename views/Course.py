# coding: utf-8
import sys
sys.path.append('../')

import time
from leancloud import LeanCloudError, Query
from flask import Blueprint, flash, render_template, request, redirect
from flask.ext.login import login_user, logout_user, current_user, login_required
from forms.UserForm import SignUpForm, LoginForm, ProfileForm
from forms.CourseForm import NewCourseForm
from models.models import Course, UserProfile

courseView = Blueprint('course', __name__)

home_mode = {
	"yue": 0,
	"qiu": 1,
	"public": 2,
	"activity": 3
}

# used for time convertion.
jinja_context = {
	'localtime' : time.localtime,
    'strftime': time.strftime
}

@courseView.route('/home/<mode>', methods=['GET'])
@login_required
def courseHome(mode):
	course_mode = 0
	if mode in home_mode:
		course_mode = home_mode[mode]
	course_list = None
	try:
		course_list = Query(Course).equal_to("user", current_user.user).equal_to("mode", course_mode).find()
	except LeanCloudError, e:
		pass
	return render_template("home_page1.html", course_list=course_list, course_mode=course_mode)

@courseView.route('/new', methods=['GET', 'POST'])
@login_required
def courseNew():
	form = NewCourseForm()
	if request.method == 'POST':
		if form.validate_on_submit():
			course = Course()
			course.set("title", form.title.data)
			course.set("info", form.info.data)
			course.set("location", form.location.data)
			course.set("time", time.mktime(form.time.data.timetuple()))
			course.set("mode", form.mode.data)
			course.set("user", current_user.user)
			course.save()
			return redirect("/course/list/yue")
		else:
			flash_errors(form)
	return render_template('home_page_request.html', form=form)

@courseView.route('/list/<mode>', methods=['GET'])
def courseList(mode):
	course_mode = 0
	if mode in home_mode:
		course_mode = home_mode[mode]
	result_list = None
	try:
		course_list =  Query(Course).equal_to("mode", course_mode).find()
		profile_list = None
		if course_list:
			users = [c.get("user") for c in course_list]
			profile_list = Query(UserProfile).contained_in("user", users).find()
			uid_map = {}
			for p in profile_list:
				uid_map[p.get("user").id] = p
			result_list = []
			for c in course_list:
				m = {"course" : c, "user" : uid_map[c.get("user").id]}
				result_list.append(m)
	except LeanCloudError, e:
		pass
	return render_template("home_qiu_list.html", result_list=result_list, course_mode=course_mode, **jinja_context)

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (getattr(form, field).label.text, error))
