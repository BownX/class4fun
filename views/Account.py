# coding: utf-8
import sys
sys.path.append('../')

from leancloud import Object, User, LeanCloudError, Query, File
from flask import Blueprint, flash, render_template, request, redirect
from flask.ext.login import login_user, logout_user, current_user, login_required, UserMixin
from forms.UserForm import SignUpForm, LoginForm, ProfileForm
from models.models import UserProfile
from app import loginManager

accountView = Blueprint('account', __name__)

class UserWrapper(UserMixin):
	""" wrap leancloud.User to support flask-login"""
	def __init__(self, user, info):
		self.user = user
		self.username = user.get('username')
		self.info = info

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return self.user.id

@accountView.route('/signup', methods=['GET', 'POST'])
def signUp():
	form = SignUpForm()
	if request.method == 'POST' and form.validate_on_submit():
		user = User()
		user.set("username", form.username.data)
		user.set("password", form.password.data)
		user.set("email", form.email.data)
		user.set("phone", form.phone.data)
		try:
			user.sign_up()
		except LeanCloudError, e:
			flash(unicode(e.error))
			return render_template('signup.html', form=form)
	return render_template('signup.html', form=form)

@accountView.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if request.method == 'POST' and form.validate_on_submit():
		user = User()
		try:
			user.login(form.username.data, form.password.data)
		except LeanCloudError, e:
			flash(unicode(e.error))
			return render_template('login.html', form=form)
		login_user(UserWrapper(user, None), remember=form.remember.data)
		next = request.args.get('next')
		return redirect(next or '/')
	return render_template('login.html', form=form)

@accountView.route('/logout', methods=['GET', 'POST'])
@login_required
def signOut():
	logout_user()
	return redirect("/")

@accountView.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
	profile = None
	try:
		profile = Query(UserProfile).equal_to('user', current_user.user).first()
	except LeanCloudError, e:
		pass
	if not profile:
		profile = UserProfile()
		profile.set('user', current_user.user)
	form = ProfileForm()
	if request.method == 'POST':
		if form.validate_on_submit():
			profile.set("realName", form.realname.data)
			profile.set("school", form.school.data)
			profile.set("major", form.major.data)
			profile.set("about", form.about.data)
			avatar_field = request.files['file']
			if avatar_field :
				avatar = File("avatar", buffer(avatar_field.read()))
				avatar.save()
				profile.set("avatar", avatar.get_thumbnail_url(width='200', height='200'))
			profile.save()
		else:
			flash_errors(form)
	return render_template('home_page_profile.html', profile=profile, form=form)

@loginManager.user_loader
def loadUser(userid):
	user = Query(User).get(userid)
	profile = None
	try:
		profile = Query(UserProfile).equal_to('user', user).first()
	except LeanCloudError, e:
		pass
	return UserWrapper(user, profile)

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (getattr(form, field).label.text, error))